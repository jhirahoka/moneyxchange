/*
 * Project: moneyxchange
 * File: environment.prod.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

export const environment = {
  production: true
};
