/*
 * Project: moneyxchange
 * File: footer-card.component.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer-card',
  templateUrl: './footer-card.component.html',
  styleUrls: ['./footer-card.component.scss']
})
export class FooterCardComponent implements OnInit {
  @Input() data;

  constructor() {
  }

  ngOnInit() {
  }

}
