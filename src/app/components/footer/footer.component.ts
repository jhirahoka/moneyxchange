/*
 * Project: moneyxchange
 * File: footer.component.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  cardData = {
    title: 'Titulo',
    detail: ' Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
  };

  constructor() {
  }

  ngOnInit() {
  }

}
