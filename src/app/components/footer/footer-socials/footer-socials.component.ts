/*
 * Project: moneyxchange
 * File: footer-socials.component.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer-socials',
  templateUrl: './footer-socials.component.html',
  styleUrls: ['./footer-socials.component.scss']
})
export class FooterSocialsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
