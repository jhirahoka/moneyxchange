/*
 * Project: moneyxchange
 * File: home-spacer.component.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-spacer',
  templateUrl: './home-spacer.component.html',
  styleUrls: ['./home-spacer.component.scss']
})
export class HomeSpacerComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
