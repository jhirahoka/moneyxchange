/*
 * Project: moneyxchange
 * File: home-calculator.component.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 11:48
 */

import {Component, OnInit} from '@angular/core';
import {FixerService} from '../../../services/fixer.service';
import {isNumber} from 'util';
import {CurrencyPipe} from '@angular/common';

@Component({
  selector: 'app-home-calculator',
  templateUrl: './home-calculator.component.html',
  styleUrls: ['./home-calculator.component.scss']
})
/**
 * TODO: Restrict input fields
 */
export class HomeCalculatorComponent implements OnInit {
  converted: string;
  original: number;
  resultCurrency = 'USD';

  constructor(private fixerService: FixerService, public currencyPipe: CurrencyPipe) {
    this.converted = this.resultCurrency;
  }

  ngOnInit() {
  }

  /**
   * TODO: Validate values
   */
  convert(value: number) {
    const convertedValue = this.fixerService.convert(value, 'USD');
    if (isNumber(convertedValue) && !isNaN(convertedValue) && convertedValue !== 0) {
      this.converted = this.currencyPipe.transform(convertedValue, this.resultCurrency, 'symbol', '0.2-4');
    } else {
      this.converted = this.resultCurrency;
    }
  }
}
