/*
 * Project: moneyxchange
 * File: rates.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

export class Rates {
  success: boolean;
  rates: any;
}
