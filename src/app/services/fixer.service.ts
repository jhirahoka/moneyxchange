/*
 * Project: moneyxchange
 * File: fixer.service.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {timer} from 'rxjs';
import {Rates} from '../models/rates';

@Injectable({
  providedIn: 'root'
})
export class FixerService {
  private refreshRate: number = 1000 * 60 * 10;
  private rates: Rates;

  constructor(private http: HttpClient) {
    timer(0, this.refreshRate)
      .subscribe(() => this._getCurrency());
  }

  public convert(value: number, currency: string) {
    return this.rates[currency] * value;
  }

  /**
   * TODO: Validate response
   */
  private _getCurrency() {
    this.http.get('http://data.fixer.io/api/latest?access_key=33b23d6e01efe285daf21f65e1124757')
      .subscribe((res: Rates) => this.rates = res.rates);
  }
}
