/*
 * Project: moneyxchange
 * File: app.component.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'moneyxchange';
}
