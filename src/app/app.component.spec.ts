/*
 * Project: moneyxchange
 * File: app.component.spec.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 09:26
 */

import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {FooterComponent} from './components/footer/footer.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeViewModule} from './modules/home-view.module';
import {FooterCardComponent} from './components/footer/footer-card/footer-card.component';
import {FooterSocialsComponent} from './components/footer/footer-socials/footer-socials.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FooterComponent,
        FooterCardComponent,
        FooterSocialsComponent
      ],
      imports: [
        AppRoutingModule,
        HomeViewModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'moneyxchange'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('moneyxchange');
  });
});
