/*
 * Project: moneyxchange
 * File: home-view.module.ts
 * Author: Jorge Hirahoka
 * Copyright (c)  2019.
 *
 * Modified: 13/09/19 11:48
 */

import {NgModule} from '@angular/core';
import {HomeViewComponent} from '../views/home-view/home-view.component';
import {HomeHeaderComponent} from '../components/home/home-header/home-header.component';
import {HomeCalculatorComponent} from '../components/home/home-calculator/home-calculator.component';
import {HomeSpacerComponent} from '../components/home/home-spacer/home-spacer.component';
import {FormsModule} from '@angular/forms';
import {CommonModule, CurrencyPipe} from '@angular/common';


@NgModule({
  declarations: [
    HomeViewComponent,
    HomeHeaderComponent,
    HomeCalculatorComponent,
    HomeSpacerComponent
  ],
  imports: [
    FormsModule,
    CommonModule
  ],
  providers: [
    CurrencyPipe
  ]
})
export class HomeViewModule {
}
